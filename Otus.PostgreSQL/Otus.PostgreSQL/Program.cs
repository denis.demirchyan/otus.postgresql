﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Otus.PostgreSQL
{
    class Program
    {
        static void Main(string[] args)
        {
            var isRun = true;
            while (isRun)
            {
                ShowMenu();
                var action = Console.ReadLine();
                switch (action)
                {
                    case "1":
                        FillDataBase();
                        break;
                    case "2":
                        PrintDataBase();
                        break;
                    case "3":
                        AddUserFromConsole();
                        break;
                    case "4":
                        AddCourseFromConsole();
                        break;
                    case "5":
                        AddLectureFromConsole();
                        break;
                    case "0":
                        isRun = false;
                        break;
                }
            }
        }

        /// <summary>
        /// Добавление пользователя из консоли
        /// </summary>
        /// <exception cref="InvalidOperationException"></exception>
        private static void AddUserFromConsole()
        {
            var user = new User();
            Console.WriteLine("Введите ФИО:");
            user.Name = Console.ReadLine();
            Console.WriteLine("Введите почту:");
            user.Email = Console.ReadLine();
            Console.WriteLine("Введите дату рождения:");
            user.BirthDay = DateTime.Parse(Console.ReadLine() ?? throw new InvalidOperationException());
            Console.WriteLine("Введите идентификатор курса:");
            user.CourseId = int.Parse(Console.ReadLine() ?? throw new InvalidOperationException());

            using var dbContext = new OtusContext();
            dbContext.Users.Add(user);
            dbContext.SaveChanges();
        }

        /// <summary>
        /// Добавление курса из консоли
        /// </summary>
        /// <exception cref="InvalidOperationException"></exception>
        private static void AddCourseFromConsole()
        {
            var course = new Course();
            Console.WriteLine("Введите название:");
            course.Name = Console.ReadLine();
            Console.WriteLine("Введите описание:");
            course.Description = Console.ReadLine();
            Console.WriteLine("Введите рейтинг:");
            course.Rating = int.Parse(Console.ReadLine() ?? throw new InvalidOperationException());
            
            using var dbContext = new OtusContext();
            dbContext.Courses.Add(course);
            dbContext.SaveChanges();
        }
        
        /// <summary>
        /// Добавление лекции из консоли
        /// </summary>
        /// <exception cref="InvalidOperationException"></exception>
        private static void AddLectureFromConsole()
        {
            var lecture = new Lecture();
            Console.WriteLine("Введите название:");
            lecture.Name = Console.ReadLine();
            Console.WriteLine("Введите описание:");
            lecture.Description = Console.ReadLine();
            Console.WriteLine("Введите автора:");
            lecture.Author = Console.ReadLine();
            Console.WriteLine("Введите идентификатор курса:");
            lecture.CourseId = int.Parse(Console.ReadLine() ?? throw new InvalidOperationException());

            using var dbContext = new OtusContext();
            dbContext.Lectures.Add(lecture);
            dbContext.SaveChanges();
        }
        
        /// <summary>
        /// Вывод меню в консоль
        /// </summary>
        private static void ShowMenu()
        {
            Console.WriteLine("Выберите действие:");
            Console.WriteLine("1. Сгенерировать тестовые данные;");
            Console.WriteLine("2. Вывести данные из таблиц;");
            Console.WriteLine("3. Добавить пользователя;");
            Console.WriteLine("4. Добавить курс;");
            Console.WriteLine("5. Добавить лекцию;");
            Console.WriteLine("0. Завершение работы;");
        }
        
        /// <summary>
        /// Вывод значений из таблиц в консоль
        /// </summary>
        private static void PrintDataBase()
        {
            using var dbContext = new OtusContext();
            Console.WriteLine("Users:");
            foreach (var user in dbContext.Users)
            {
                Console.WriteLine(user.ToString());
            }
            Console.WriteLine("Courses:");
            foreach (var course in dbContext.Courses)
            {
                Console.WriteLine(course.ToString());
            }
            Console.WriteLine("Lectures:");
            foreach (var lecture in dbContext.Lectures)
            {
                Console.WriteLine(lecture.ToString());
            }
        }
        
        /// <summary>
        /// Заполнение тестовыми данными бд
        /// </summary>
        private static void FillDataBase()
        {
            using var dbContext = new OtusContext();
            var random = new Random();

            for (var i = 0; i < random.Next(5, 10); ++i)
            {
                var course = new Course
                {
                    Name = RandomString(), Description = RandomString(), Rating = random.Next(1, 100),
                    Lectures = new List<Lecture>(), Users = new List<User>()
                };

                for (var j = 0; j < random.Next(1, 10); ++j)
                {
                    course.Lectures.Add(new Lecture
                        {Name = RandomString(), Description = RandomString(), Author = RandomString()});
                }

                for (var j = 0; j < random.Next(1, 10); ++j)
                {
                    course.Users.Add(
                        new User {Name = RandomString(), Email = RandomString(), BirthDay = DateTime.Today});
                }
                dbContext.Add(course);
            }

            dbContext.SaveChanges();
        }

        /// <summary>
        /// Генерация рандомной строки
        /// </summary>
        /// <returns></returns>
        private static string RandomString()
        {
            var random = new Random();
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, random.Next(6, 15))
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}