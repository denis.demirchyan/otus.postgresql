using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Otus.PostgreSQL
{
    public sealed class OtusContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Lecture> Lectures { get; set; }

        public OtusContext()
        {
            Database.EnsureCreated();
        }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder
                .UseNpgsql("Host=localhost;Port=5432;Database=otus;Username=pguser;Password=pgpassword")
                .UseSnakeCaseNamingConvention();
    }

    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public DateTime BirthDay { get; set; }
        
        public int CourseId { get; set; }
        public Course Course { get; set; }

        public override string ToString()
        {
            return $"Id: {Id.ToString().PadRight(8)}|\t" +
                   $"Name: {Name.PadRight(30)}|\t" +
                   $"Email: {Email.PadRight(30)}|\t" +
                   $"BirthDay: {BirthDay.Date.ToString(CultureInfo.CurrentCulture).PadRight(30)}|\t" +
                   $"Course: {CourseId.ToString().PadRight(8)}|";
        }
    }

    public class Course
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Rating { get; set; }
        public List<Lecture> Lectures { get; set; }
        public List<User> Users { get; set; }

        public override string ToString()
        {
            return $"Id: {Id.ToString().PadRight(8)}|\t" +
                   $"Name: {Name.PadRight(30)}|\t" +
                   $"Description: {Description.PadRight(30)}|\t" +
                   $"Rating: {Rating.ToString().PadRight(8)}|\t";
        }
    }

    public class Lecture
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Author { get; set; }

        public int CourseId { get; set; }
        public Course Course { get; set; }
        
        public override string ToString()
        {
            return $"Id: {Id.ToString().PadRight(8)}|\t" +
                   $"Name: {Name.PadRight(30)}|\t" +
                   $"Description: {Description.PadRight(30)}|\t" +
                   $"Author: {Author.PadRight(30)}|\t" +
                   $"Course: {CourseId.ToString().PadRight(8)}|";
        }
    }
}